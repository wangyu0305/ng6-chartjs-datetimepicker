import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { HttpClient, HttpHeaders, HttpHandler} from '@angular/common/http';

@Component({
  selector: 'app-angular-data-table',
  templateUrl: './angular-data-table.component.html',
  styleUrls: ['./angular-data-table.component.css']
})
export class AngularDataTableComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['id', 'envName', 'target', 'startTime', 'endTime', 'enabled', 'creatorFullName',
   'createdAt', 'updaterFullName', 'lastModifiedAt', 'reqIns'];

  date = new Date();
  fakeData: PeriodicElement[] = [
    {
      'id': '123',
      'envName': 'envName',
      'target': 'target',
      'startTime': 'this.date',
      'endTime' : 'this.date',
      'enabled': 'this.actions(element)',
      'creatorFullName': 'creatorFullName',
      'createdAt': 'this.date',
      'updaterFullName': 'updaterFullName',
      'lastModifiedAt': 'this.date',
      'reqIns': 'reqIns'
    }
  ];
  ELEMENT_DATA: PeriodicElement[];
  dataSource;

   private requestData = {
    'columns': [],
    'draw': 1,
    'length': 1,
    'order': [],
    'search': {},
    'start': 0
  };


   @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<PeriodicElement>(this.fakeData);
    this.dataSource.paginator = this.paginator;
    this.ELEMENT_DATA = this.fakeData;
    // this.init();
  }

  init() {
    const thisheader = new HttpHeaders(
      {'Cookie':
      'JSESSIONID=0D35AACA6FD2B5EFF718E558B5C2D61F; remember-me=QitqaFJVaW05aFRoU091WTJhZFlpdz09Om9RSko4bWsxYUNMckc1c3Z5cUpoYWc9PQ'}
    );
    this.http.post('http://localhost:4200/api/maintenance/list', this.requestData, {headers: thisheader}).subscribe( (response) => {
    console.log(response);
    this.ELEMENT_DATA = this.createDataTable(response);
    }, err => {
      console.log(err);
    });
  }

  createDataTable(data) {
    const dataTabel = [];
    data['data'].forEach(element => {
        dataTabel.push({
            'id': element['id'],
            'envName': element['envName'],
            'target': element['target'],
            'startTime': element['startTime'],
            'endTime' : element['endTime'],
            'enabled': this.actions(element),
            'creatorFullName': element['creatorFullName'],
            'createdAt': element['createdAt'],
            'updaterFullName': element['updaterFullName'],
            'lastModifiedAt': element['lastModifiedAt'],
            'reqIns': element['reqIns']
        });
    });
    return dataTabel;
}

actions(full) {
  const id = full.id;
  let checked = '';
  if (full.enabled) {
      checked = 'checked';
  }
  return  '<input type="checkbox" ng-click="disableSchedule(' + id + ')" ' + checked + '> Enabled';
}

}

export interface PeriodicElement {
  id: string;
  envName: string;
  target: string;
  startTime: string;
  endTime: string;
  enabled: string;
  creatorFullName: string;
  createdAt: string;
  updaterFullName: string;
  lastModifiedAt: string;
  reqIns: string;
}
