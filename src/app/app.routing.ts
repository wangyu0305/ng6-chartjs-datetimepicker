
import { RouterModule, Routes } from '@angular/router';
import {UserComponent} from './user/user.component';
import {LoginComponent} from './login/login.component';
import {ChartComponent} from './chart/chart.component';
import {DatatablesComponent} from './datatables/datatables.component';
import {DataTableRerenderComponent} from './data-table-rerender/data-table-rerender.component';
import {AngularDataTableComponent} from './angular-data-table/angular-data-table.component';

const routes: Routes = [
{ path: 'user', component: UserComponent },
{ path: 'login', component: LoginComponent },
{ path: 'chart', component: ChartComponent },
{ path: 'table', component: DatatablesComponent },
{ path: 'tablererender', component: DataTableRerenderComponent },
{ path: 'ngtables', component: AngularDataTableComponent },
{path : '', component : LoginComponent}
];

export const routing = RouterModule.forRoot(routes);
