import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import { ValidationService } from '../validation.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm: FormGroup;
  submitted: Boolean = false;
  invalidLogin: Boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private validService: ValidationService) { }

  ngOnInit() {
    this.registerForm = this.validService.loginValid();
    // this.registerForm = this.formBuilder.group({
    //   firstName: ['', Validators.required],
    //   lastName: ['', Validators.required],
    //   email: ['', [Validators.required, Validators.email]],
    //   password: ['', [Validators.required, Validators.minLength(6)]]
    // });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    if (this.registerForm.controls.email.value === 'test@gmail.com' && this.registerForm.controls.password.value === 'password') {
        console.log('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value));
        this.router.navigate(['user']);
    } else {
      console.log('FAIL!! :-)\n\n' + JSON.stringify(this.registerForm.value));
      this.invalidLogin = true;
    }
  }
}
