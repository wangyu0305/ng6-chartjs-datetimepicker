import { Component, OnInit } from '@angular/core';
import { LoggerService } from 'ng-logger';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private log: LoggerService) { }

  ngOnInit() {
    this.log.logConfig =  {
      threshold: 'DEBUG',
      colorOutput: true
    };

    this.log.info('Log level set to: ', this.log.logLevel);
  }
}
