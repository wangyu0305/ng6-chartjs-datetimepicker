import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-datatables',
  templateUrl: './datatables.component.html',
  styleUrls: ['./datatables.component.css']
})

export class DatatablesComponent implements OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  min: number;
  max: number;

  dtOptions: DataTables.Settings = {};

  constructor(private cookieService: CookieService) { }

  ngOnInit(): void {
    // this.cookieService.set( 'Test', 'Hello World' );

    const allCookies: {} = this.cookieService.getAll();
    console.warn(allCookies);

    $.fn['dataTable'].ext.search.push((data, dataIndex) => {
      const id = parseFloat(data[0]) || 0;
      if ((isNaN(this.min) && isNaN(this.max)) ||
        (isNaN(this.min) && id <= this.max) ||
        (this.min <= id && isNaN(this.max)) ||
        (this.min <= id && id <= this.max)) {
        return true;
      }
      return false;
    });

    this.dtOptions = {
      data: [
        {
          'isPersist': '<input type="checkbox" name="feature" value="scales" checked />',
          'requestId': 5235235,
          'checkResult': '<div class="badge badge-success">8</div>"/"<div class="badge badge-secondary">8</div>',
          'event': '<div class="progress">' +
          '<span class="progress-bar bg-success" role="progressbar" style="width:80%">server: ws-03 last name start from here</span>' +
          '<span class="progress-bar bg-info" role="progressbar" style="width:20%">Warning</span></div>'
        },
        {
          'isPersist': '<input type="checkbox" name="feature1" value="scales1" checked />',
          'requestId': 3235235,
          'checkResult': 'Whateveryournameis',
          'event': 'Yoda'
        },
        {
          'isPersist': '<input type="checkbox" name="feature2" value="scales2" checked />',
          'requestId': 62363626,
          'checkResult': '<div class="badge badge-success">3</div>"/"<div class="badge badge-secondary">3</div>',
          'event': 'Yoda'
        }
      ],
      columns: [{
        title: 'Is Persist',
        data: 'isPersist'
      }, {
        title: 'Request ID',
        data: 'requestId'
      }, {
        title: 'Check Result',
        data: 'checkResult'
      }, {
        title: 'Event',
        data: 'event'
      }]
    };
  }

  ngOnDestroy(): void {
    $.fn['dataTable'].ext.search.pop();
  }

  filterById(): void {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

}
