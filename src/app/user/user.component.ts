import { Component, OnInit } from '@angular/core';
import { LoggerService } from 'ng-logger';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public min = new Date(2018, 1, 12, 10, 30);

  // Max moment: April 21 2018, 20:30
  public max = new Date(2018, 3, 21, 20, 30);

  constructor(private log: LoggerService) { }

  ngOnInit() {
    const myVar = {
      'userId': '1232321',
      'name': 'asdsdsa',
      'password': 'secret',
      'firstname': 'first',
      'lastname': 'last',
      'authtication': 'secret'
    };
    this.log.debug('Logging works!:', myVar);
    this.log.info('Logging works!:', myVar);
    this.log.warn('Logging works!:', myVar);
    this.log.error('Logging works!:', myVar);
  }

}
