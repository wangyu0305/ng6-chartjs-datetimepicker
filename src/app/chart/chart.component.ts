import { Component, AfterViewInit } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataService } from '../data.service';
import { ErrorsHandler } from '../error/errors-handler';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent implements AfterViewInit {

  private Url = 'http://192.168.1.45:8050/api/dashboard/applications/2018-06-28%2011:59:58/0';

  constructor(private http: HttpClient, private data: DataService, private err: ErrorsHandler) { }

  public fakedata: Array<any> = this.data.fakeData.result;

  public colors = ['#3e95cd', '#8e5ea2', '#3cba9f', '#e8c3b9', '#c45850', '#ffcc00'];
  private labels = [
        'failedServerCount',
        'hideServerCount',
        'infoServerCount',
        'maintenanceServerCount',
        'passedServerCount',
        'warningServerCount'
      ];

  ngAfterViewInit() {
    this.err.handleError(this.getJsonData());
    // this.getJsonData();
    for (const fdata of this.fakedata) {
      this.newChart(
        fdata.displayName,
          this.labels,
          [
            fdata.failedServerCount,
            fdata.hideServerCount,
            fdata.infoServerCount,
            fdata.maintenanceServerCount,
            fdata.passedServerCount,
            fdata.warningServerCount
          ],
          fdata.name,
          this.colors
        );
    }
  }

  getJsonData(): any {
    let hder = new HttpHeaders();
    hder.append('Content-Type', 'application/json');
    hder.append('Authorization', 'Basic ' + btoa('ken:Not4u2know'));
    const options = { headers: hder };
    this.http.get(this.Url, options).subscribe((data: any) => {
          const info = JSON.parse(data);
          console.log(info);
       }, err => {
          console.log('User authentication failed!', err);
       });
  }

  newChart(chartID, labelsArray, data, label, bColors) {
    return new Chart(document.getElementById(chartID), {
      type: 'pie',
      data: {
          labels: labelsArray,
          datasets: [{
              label: label,
              data: data,
              backgroundColor: bColors,
              borderWidth: 1
          }]
      },
      options: {
        legend: {
          display: false
        },
        responsive: false,
        display: true
      }
    });
  }

}
