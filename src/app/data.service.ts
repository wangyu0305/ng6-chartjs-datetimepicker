import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  fakeData = {
    'code': '0',
    'message': '',
    'result': [
      {
        'description': 'This is a demo onlinebanking application',
        'displayName': 'Copy-OnlineBanking',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 23,
        'groupName': 'Your-application-grp',
        'hideServerCount': 7,
        'id': 187,
        'infoServerCount': 0,
        'lastCheckTime': '2018-06-27T20:52:46.183+0000',
        'maintenanceServerCount': 0,
        'name': 'Copy-OnlineBanking-187',
        'numberOfServer': 8,
        'passedServerCount': 1,
        'sortedOrder': 0,
        'warningServerCount': 0
      },
      {
        'description': 'This is a demo online bankingapplication',
        'displayName': 'OnlineBanking',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 16,
        'groupName': 'SafeBanking',
        'hideServerCount': 8,
        'id': 67,
        'infoServerCount': 0,
        'lastCheckTime': '2018-05-31T21:14:22.101+0000',
        'maintenanceServerCount': 0,
        'name': 'OnlineBanking',
        'numberOfServer': 8,
        'passedServerCount': 0,
        'sortedOrder': 0,
        'warningServerCount': 0
      },
      {
        'description': 'OnlineBanking Service',
        'displayName': 'OnlineBankingService',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 16,
        'groupName': 'SafeBanking',
        'hideServerCount': 2,
        'id': 105,
        'infoServerCount': 0,
        'lastCheckTime': '2018-05-31T21:13:41.842+0000',
        'maintenanceServerCount': 0,
        'name': 'New_Profile_Service',
        'numberOfServer': 2,
        'passedServerCount': 0,
        'sortedOrder': 1,
        'warningServerCount': 0
      },
      {
        'description': 'Profile_Service_LB',
        'displayName': 'OnlineBanking ServiceLB',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 16,
        'groupName': 'SafeBanking',
        'hideServerCount': 1,
        'id': 108,
        'infoServerCount': 0,
        'lastCheckTime': '2018-05-31T21:13:41.843+0000',
        'maintenanceServerCount': 0,
        'name': 'Profile_Service_LB',
        'numberOfServer': 1,
        'passedServerCount': 0,
        'sortedOrder': 1,
        'warningServerCount': 0
      },
      {
        'description': 'OnlineBanking DBCheck',
        'displayName': 'OnlineBanking DBCheck',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 16,
        'groupName': 'SafeBanking',
        'hideServerCount': 1,
        'id': 110,
        'infoServerCount': 0,
        'lastCheckTime': '2018-05-31T21:13:41.842+0000',
        'maintenanceServerCount': 0,
        'name': 'OnlineBanking DBCheck',
        'numberOfServer': 1,
        'passedServerCount': 0,
        'sortedOrder': 5,
        'warningServerCount': 0
      },
      {
        'description': 'Confluent Kafka',
        'displayName': 'ConfluentKafka',
        'failedServerCount': 0,
        'freqInstruction': '5m',
        'groupId': 16,
        'groupName': 'SafeBanking',
        'hideServerCount': 1,
        'id': 111,
        'infoServerCount': 0,
        'lastCheckTime': '2018-05-29T22:24:20.462+0000',
        'maintenanceServerCount': 0,
        'name': 'ConfluentKafka',
        'numberOfServer': 1,
        'passedServerCount': 0,
        'sortedOrder': 1000,
        'warningServerCount': 0
      }
    ]
  };
}
