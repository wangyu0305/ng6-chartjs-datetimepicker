import { ErrorHandler, Injectable} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class ErrorsHandler implements ErrorHandler {
  handleError(error: Error | HttpErrorResponse) {
   if (error instanceof HttpErrorResponse) {
      // Server or connection error happened
      if (!navigator.onLine) {
        console.log('Handle offline error', error.name, error.message);
      } else {
        console.log('Handle Http Error (error.status === 403, 404...)', error.name, error.message);
      }
   } else {
     // Handle Client Error (Angular Error, ReferenceError...)
     console.log('Handle offline error', error.name, error.message);
   }
  // Log the error anyway
  console.error('It happens: ', error);
}
}
