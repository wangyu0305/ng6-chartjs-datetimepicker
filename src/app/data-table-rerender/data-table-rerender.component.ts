import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-data-table-rerender',
  templateUrl: './data-table-rerender.component.html',
  styleUrls: ['./data-table-rerender.component.css']
})
export class DataTableRerenderComponent implements AfterViewInit, OnDestroy, OnInit {

  constructor() { }

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: Subject<any> = new Subject();

  ngOnInit(): void {
    this.dtOptions = {
      data: [{
        'id': 1,
        'envName': 'name',
        'target': 'target',
        'startTime': 'starttime',
        'endTime': 'endtime',
        'enabled': 'enabled',
        'creatorFullName': 'fullname',
        'createdAt': 'at',
        'updaterFullName': 'fullname',
        'lastModifiedAt': 'lastname',
        'reqIns': 'ins'
    }],
    columns: [{
      title: 'ID',
      data: 'id'
    }, {
      title: 'Environment',
      data: 'envName'
    }, {
      title: 'Target',
      data: 'target'
    }, {
      title: 'Start Time',
      data: 'startTime'
    }, {
        title: 'End Time',
        data: 'endTime'
      }, {
        title: 'Enabled',
        data: 'enabled'
      }, {
        title: 'Created By',
        data: 'creatorFullName'
      }, {
        title: 'Created At',
        data: 'createdAt'
      }, {
        title: 'Last Modified By',
        data: 'updaterFullName'
      }, {
        title: 'Last Modified At',
        data: 'lastModifiedAt'
      }, {
        title: 'Submission Note',
        data: 'reqIns'
      }]
    };
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

}
