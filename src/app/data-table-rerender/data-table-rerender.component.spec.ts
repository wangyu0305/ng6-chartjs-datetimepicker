import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableRerenderComponent } from './data-table-rerender.component';

describe('DataTableRerenderComponent', () => {
  let component: DataTableRerenderComponent;
  let fixture: ComponentFixture<DataTableRerenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTableRerenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableRerenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
