import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import {routing} from './app.routing';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ChartComponent } from './chart/chart.component';
import { DatatablesComponent } from './datatables/datatables.component';
import { DataTablesModule } from 'angular-datatables';
import { CookieService } from 'ngx-cookie-service';
import { DataTableRerenderComponent } from './data-table-rerender/data-table-rerender.component';
import { AngularDataTableComponent } from './angular-data-table/angular-data-table.component';
import {MatTableModule} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import { LoggerService } from 'ng-logger';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    ChartComponent,
    DatatablesComponent,
    DataTableRerenderComponent,
    AngularDataTableComponent
  ],
  imports: [
    BrowserModule,
    routing,
    ReactiveFormsModule,
    HttpClientModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    DataTablesModule,
    MatTableModule,
    CdkTableModule,
  ],
  providers: [ CookieService, LoggerService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
